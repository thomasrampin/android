package aixcom.iut.r13007071.gameproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button single = (Button) findViewById(R.id.buttonOne);
        single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lancerSingle = new Intent(MainActivity.this, QuizSingle.class);
                startActivity(lancerSingle);
            }
        });

        Button multiplayer = (Button) findViewById(R.id.buttonMulti);
        multiplayer.setOnClickListener(new View.OnClickListener() {
            int count = 0;
            @Override
            public void onClick(View v) {
                if( (count % 2) == 0 ){
                    count++;
                    Button two = (Button) findViewById(R.id.buttonTwo);
                    two.setVisibility(View.VISIBLE);
                    Button three = (Button) findViewById(R.id.buttonThree);
                    three.setVisibility(View.VISIBLE);
                    Button four = (Button) findViewById(R.id.buttonFour);
                    four.setVisibility(View.VISIBLE);
                    single.setVisibility(View.INVISIBLE);

                    two.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent lancerTwo = new Intent(MainActivity.this, QuizTwo.class);
                            startActivity(lancerTwo);
                        }
                    });

                    three.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent lancerThree = new Intent(MainActivity.this, QuizThree.class);
                            startActivity(lancerThree);
                        }
                    });

                    four.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent lancerFour = new Intent(MainActivity.this, QuizFour.class);
                            startActivity(lancerFour);
                        }
                    });

                } else {
                    count++;
                    Button two = (Button) findViewById(R.id.buttonTwo);
                    two.setVisibility(View.INVISIBLE);
                    Button three = (Button) findViewById(R.id.buttonThree);
                    three.setVisibility(View.INVISIBLE);
                    Button four = (Button) findViewById(R.id.buttonFour);
                    four.setVisibility(View.INVISIBLE);
                    single.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
