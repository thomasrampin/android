package fr.univ.amu.iut.iuttest;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity {
	
	private final static String TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onCreate ");
		}
		
		Log.v(TAG, "onCreate ");
		Log.d(TAG, "onCreate ");
		Log.i(TAG, "onCreate ");
		Log.w(TAG, "onCreate ");
		Log.e(TAG, "onCreate ");
		Log.wtf(TAG, "onCreate ");
	}
	
	// START PROCESS
	@Override
	public void onStart() {
		super.onStart();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onStart ");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onResume ");
		}
	}

	@Override
	protected void onRestart() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onRestart ");
		}
	};

	// DESTROY PROCESS

	@Override
	public void onPause() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onPause ");
		}
		super.onPause();
	}

	@Override
	public void onStop() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onStop ");
		}
		super.onStop();
	}

	@Override
	public void onDestroy() {
		if (BuildConfig.DEBUG) {
			Log.d(TAG, "onDestroy ");
		}
		super.onDestroy();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
