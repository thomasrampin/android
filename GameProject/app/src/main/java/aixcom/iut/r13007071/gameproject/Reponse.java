package aixcom.iut.r13007071.gameproject;

/**
 * Created by r13007071 on 07/04/15.
 */
public class Reponse {

    private int numRep;
    private String reponse;

    public Reponse(int numRep, String reponse){
        this.numRep = numRep;
        this.reponse = reponse;
    }
}
