package aix.iut.r13007071.lastcourse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity2 extends Activity {

    public static final String ACTIVITY_DOS_MSG = "retour du turfu";

    private EditText editText1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);

        editText1=(EditText)findViewById(R.id.editText1);

        Intent intent = getIntent();
        Toast toast = Toast.makeText(getApplicationContext(),intent.getStringExtra(MainActivity.EXTRA_MESG),Toast.LENGTH_SHORT);
        toast.show();

        Button buttonDos = (Button) findViewById(R.id.button2);

        buttonDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = editText1.getText().toString();
                Intent intentdos = new Intent();
                intentdos.putExtra("MSG",message);
                setResult(2,intentdos);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
