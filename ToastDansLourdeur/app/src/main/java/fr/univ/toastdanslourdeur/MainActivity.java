package fr.univ.toastdanslourdeur;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
	
	private OnClickListener clickListenerBut =new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Toast.makeText(getApplicationContext(), "IMC OKLM", Toast.LENGTH_SHORT ).show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Toast.makeText(getApplicationContext(), R.string.app_name, Toast.LENGTH_SHORT ).show();
		Button reset = (Button) findViewById(R.id.button1);
		Button imcCalc = (Button) findViewById(R.id.button2);
		
	    final EditText etPoids = (EditText) findViewById(R.id.editText1);
		final EditText etTaille = (EditText)findViewById(R.id.editText2);

        final RadioGroup rgMetric = (RadioGroup) findViewById(R.id.radioGroup);


        imcCalc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String poids = etPoids.getText().toString();
                String taille = etTaille.getText().toString();

                long valuePoids = Long.parseLong(poids);
                long valueTaille = Long.parseLong(taille);

                if(rgMetric.getCheckedRadioButtonId() == R.id.radioButton2){
                    valueTaille = valueTaille / 100;
                }

                String result = Float.toString(calcuationImc(valuePoids,valuePoids));
                Toast.makeText(getApplication(),poids +" "+ taille+ " IMC: " + result,Toast.LENGTH_LONG).show();
            }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {


		
	}

    public float calcuationImc(long poids, long taille){
        return poids/(taille*taille);
    }

}
