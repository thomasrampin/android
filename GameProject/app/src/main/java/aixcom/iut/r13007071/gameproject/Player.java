package aixcom.iut.r13007071.gameproject;

/**
 * Created by r13007071 on 02/04/15.
 */
public class Player {

    //private String pseudo;
    private int score;
    private  int number;

    public Player(/*String pseudo,*/ int score, int number){
        this.score = score;
        this.number = number;
    }

    public void setScore(int scoreBase){
        this.score = scoreBase;
    }

    public void setNumber(int numberPla){
        this.number = numberPla;
    }

    public int getNumber() {
        return number;
    }

    public int getScore() {
        return score;
    }
}
